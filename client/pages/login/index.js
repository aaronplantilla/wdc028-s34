import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../../UserContext';
import AppHelper from '../../app-helper'
import View from '../../components/View'

export default function index() {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}

const LoginForm = () => {
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        
        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }
    
    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
            console.log(data)
            setUser({ email: data.email, isAdmin: data.isAdmin })
            Router.push('/courses')
        })
    }

    const authenticateGoogleToken = (response) => {

        setTokenId(response.tokenId)

        const payload = {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${ AppHelper.API_URL }/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control type="email" value={ email } onChange={ (e) => setEmail(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={ password } onChange={ (e) => setPassword(e.target.value) } required/>
                    </Form.Group>
                    <Button className="mb-2" variant="success" type="submit" block>Login</Button>
                    <GoogleLogin
                        clientId="127553766881-e4v2v6lc881gk4jocdq9lkllhro6lhps.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                </Form>
            </Card.Body>
        </Card>
    )
}