const User = require("../models/user")
const Course = require("../models/course")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const { OAuth2Client } = require('google-auth-library')
const clientId = '127553766881-e4v2v6lc881gk4jocdq9lkllhro6lhps.apps.googleusercontent.com'
const sms = require('../sms')
const nodemailer = require('nodemailer')
require('dotenv').config();

//check if email already exists
module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false;
	})
}

//registration
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		//hashSync() encrypts the password, and the 10 makes it happen 10 times
		loginType: 'email'
	})

	return newUser.save().then((user, err) => {
		return (err) ? false: true
	})
}

//login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne === null){ //user does not exist
			return { error: 'does-not-exist' }
		}

		if (resultFromFindOne.loginType !== 'email') { 
        	return { error: 'login-type-error' }
        }

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		//compareSync() is used to compare a non-hashed password to a hashed password, and it returns a boolean value (true or false)

		if(isPasswordMatched){
			return {accessToken: auth.createAccessToken(resultFromFindOne.toObject())}
			//auth is a custom module that uses JSON web token to pass data around 
			//createAccessToken is a method in auth that creates a JWT with the user's information as part of the token
		}else{
			return { error: 'incorrect-password' }
		}

	})
}

//get user profile
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}

//enroll to a class
module.exports.enroll = (params) => {
	return User.findById(params.userId).then(resultFromEnrollSearch => {
		resultFromEnrollSearch.enrollments.push({courseId: params.courseId})
		//pushes the courseId to the user's enrollments array (student is now enrolled)

		return resultFromEnrollSearch.save().then((resultFromSaveUser, err) => {
			return Course.findById(params.courseId).then(resultFromFindByIdCourse => {
				resultFromFindByIdCourse.enrollees.push({userId: params.userId})
				//pushes the userId to the course's enrollees array

				return resultFromFindByIdCourse.save().then((resultFromSaveCourse, err) => {
					if(err){
						return false
					}else{
						// sms.send(resultFromEnrollSearch.mobileNo, `Good day! At ${ new Date().toLocaleString() }, you have enrolled on a course entitle ${ resultFromFindByIdCourse.name }. Enjoy the course and learn continuously!`)

						const mailOptions = {
							from: 'coursebooking@gmail.com',
							// to: resultFromEnrollSearch.email
							to: 'aaronplantilla@gmail.com',
							subject: 'Course Booking Enrollment',
							text: `Good day! At ${ new Date().toLocaleString() }, you have enrolled on a course entitled ${ resultFromFindByIdCourse.name }. Enjoy the course and learn continuously!`,
							html: `<h1>Good day! At ${ new Date().toLocaleString() }, you have enrolled on a course entitled ${ resultFromFindByIdCourse.name }. Enjoy the course and learn continuously!</h1>`,
						}

						function sendMail(transporter) {
							transporter.sendMail(mailOptions,function(err,result){
								if(err){
									return { message: err }
								}else{
									transport.close();
									return console.log('Email has been sent: check your inbox!')
								}
							})
						}

						 if (resultFromEnrollSearch.loginType === 'email'){
                        	const transporter = nodemailer.createTransport({
							    host: 'smtp.ethereal.email',
							    port: 587,
							    secure: false,
							    auth: {
							        user: 'etherealmail',
							        pass: 'etherealpass'
							    }
							});

       						sendMail(transporter)

                        }else if(resultFromEnrollSearch.loginType === 'google'){
                        	const transporter = nodemailer.createTransport({
							    host: 'smtp.gmail.com',
							    port: 465,
							    secure: true,
								auth: {
							        type: 'OAuth2',
							        user: process.env.MAILING_SERVICE_EMAIL,
							        clientId: process.env.MAILING_SERVICE_CLIENT_ID,
							        clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
							        refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
							        accessToken: params.token
							    }
							});

							sendMail(transporter)
                        }

                        return true

					}
				})
			})
		})
	})
}

// Google login
module.exports.verifyGoogleTokenId = async (tokenId) => {
    const client = new OAuth2Client(clientId);
    const data = await client.verifyIdToken({
        idToken: tokenId,
        audience: clientId,
    });

    if (data.payload.email_verified === true) {
    const user = await User.findOne({ email: data.payload.email }).exec();

        if (user !== null) {
            if (user.loginType === "google") {
                return { accessToken: auth.createAccessToken(user.toObject()) };
            } else {
                return { error: "login-type-error" };
            }
        } else {
            const newUser = new User({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email: data.payload.email,
                loginType: "google",
            });

            return newUser.save().then((user, err) => {
                return { accessToken: auth.createAccessToken(user.toObject()) };
            });
        }
    } else {
        return { error: "google-auth-error" };
    }
};