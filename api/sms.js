const request = require('request');

module.exports.send = (mobileNo, message) => {
	const shortCode = '21583584'
	const accessToken = 'vl_z0zn97iG5UHsiTJHplB5Jwj6l8VpeMUbgx3Ch7UA'
	const clientCorrelator = '123576'

    const options = {
	    method: 'POST',
	    url: 'https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/' + shortCode + '/requests',
	    qs: { 'access_token': accessToken },
	    headers: { 'Content-Type': 'application/json' },
	    body: {
	        'outboundSMSMessageRequest': { 
	            'clientCorrelator': clientCorrelator,
	            'senderAddress': shortCode,
	            'outboundSMSTextMessage': { 'message': message },
	            'address': mobileNo, 
	        } 
	    },
	    json: true 
	}
    request(options, (error, response, body) => {
        if (error) throw new Error(error)
        console.log(body)
    })
}