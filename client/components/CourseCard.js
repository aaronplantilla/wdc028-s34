import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import AppHelper from '../app-helper';

export default function CourseCard({courseProp}){
	const {name, description, price, start_date, end_date} = courseProp;

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);
	const [isOpen, setIsOpen] = useState(true);

	function enroll(courseId){

		fetch(`${ AppHelper.API_URL }/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)
		})

	}

	useEffect(() => {
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats])

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<span className="subtitle">Description</span>
					<br />
					{description}
					<br />
					<span className="subtitle">Price</span>
					<br />
					&#8369; {price}
					<br />
					<span className="subtitle">Start Date</span>
					<br />
					{start_date}
					<br />
					<span className="subtitle">End Date</span>
					<br />
					{end_date}
					<br />
					<span className="subtitle">Enrollees</span>
					<br />
					{count}
					<br />
					<span className="subtitle">Seats</span>
					<br />
					{seats}
				</Card.Text>
				{isOpen ?
					<Button className="bg-primary" onClick={() => {enroll(courseProp._id)}}>Enroll</Button>
					:
					<Button className="bg-danger" disabled>Not Available</Button>
				}
			</Card.Body>
		</Card>
	)
}

CourseCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}